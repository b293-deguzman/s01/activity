package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {

    void createUser(User user);

    Optional<User> findByUsername(String username);

    //get specific user from token
    Object getUser(String stringToken);


}
